<?php
namespace HIVE\HiveExtContact\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 * @author Yannick Aister <y.aister@teufels.com>
 */
class DepartmentTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtContact\Domain\Model\Department
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtContact\Domain\Model\Department();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBackendTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendTitle()
        );
    }

    /**
     * @test
     */
    public function setBackendTitleForStringSetsBackendTitle()
    {
        $this->subject->setBackendTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendTitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCompanyReturnsInitialValueForCompany()
    {
        self::assertEquals(
            null,
            $this->subject->getCompany()
        );
    }

    /**
     * @test
     */
    public function setCompanyForCompanySetsCompany()
    {
        $companyFixture = new \HIVE\HiveExtContact\Domain\Model\Company();
        $this->subject->setCompany($companyFixture);

        self::assertAttributeEquals(
            $companyFixture,
            'company',
            $this->subject
        );
    }
}
