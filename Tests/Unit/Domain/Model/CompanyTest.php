<?php
namespace HIVE\HiveExtContact\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 * @author Yannick Aister <y.aister@teufels.com>
 */
class CompanyTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtContact\Domain\Model\Company
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtContact\Domain\Model\Company();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForStringSetsCity()
    {
        $this->subject->setCity('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'city',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZipReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getZip()
        );
    }

    /**
     * @test
     */
    public function setZipForStringSetsZip()
    {
        $this->subject->setZip('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'zip',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStreetReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStreet()
        );
    }

    /**
     * @test
     */
    public function setStreetForStringSetsStreet()
    {
        $this->subject->setStreet('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'street',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRegionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRegion()
        );
    }

    /**
     * @test
     */
    public function setRegionForStringSetsRegion()
    {
        $this->subject->setRegion('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'region',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCountryReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCountry()
        );
    }

    /**
     * @test
     */
    public function setCountryForStringSetsCountry()
    {
        $this->subject->setCountry('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'country',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWwwReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getWww()
        );
    }

    /**
     * @test
     */
    public function setWwwForStringSetsWww()
    {
        $this->subject->setWww('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'www',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLatReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLat()
        );
    }

    /**
     * @test
     */
    public function setLatForStringSetsLat()
    {
        $this->subject->setLat('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'lat',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLonReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLon()
        );
    }

    /**
     * @test
     */
    public function setLonForStringSetsLon()
    {
        $this->subject->setLon('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'lon',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSocialMediaReturnsInitialValueForSocialMedia()
    {
        self::assertEquals(
            null,
            $this->subject->getSocialMedia()
        );
    }

    /**
     * @test
     */
    public function setSocialMediaForSocialMediaSetsSocialMedia()
    {
        $socialMediaFixture = new \HIVE\HiveExtContact\Domain\Model\SocialMedia();
        $this->subject->setSocialMedia($socialMediaFixture);

        self::assertAttributeEquals(
            $socialMediaFixture,
            'socialMedia',
            $this->subject
        );
    }
}
