<?php
namespace HIVE\HiveExtContact\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 * @author Yannick Aister <y.aister@teufels.com>
 */
class ContactTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtContact\Domain\Model\Contact
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtContact\Domain\Model\Contact();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getGenderReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getGender()
        );
    }

    /**
     * @test
     */
    public function setGenderForIntSetsGender()
    {
        $this->subject->setGender(12);

        self::assertAttributeEquals(
            12,
            'gender',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFirstnameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFirstname()
        );
    }

    /**
     * @test
     */
    public function setFirstnameForStringSetsFirstname()
    {
        $this->subject->setFirstname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'firstname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMiddlenameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMiddlename()
        );
    }

    /**
     * @test
     */
    public function setMiddlenameForStringSetsMiddlename()
    {
        $this->subject->setMiddlename('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'middlename',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLastnameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLastname()
        );
    }

    /**
     * @test
     */
    public function setLastnameForStringSetsLastname()
    {
        $this->subject->setLastname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'lastname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFocusXReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFocusX()
        );
    }

    /**
     * @test
     */
    public function setFocusXForStringSetsFocusX()
    {
        $this->subject->setFocusX('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'focusX',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFocusYReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFocusY()
        );
    }

    /**
     * @test
     */
    public function setFocusYForStringSetsFocusY()
    {
        $this->subject->setFocusY('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'focusY',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBirthdateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getBirthdate()
        );
    }

    /**
     * @test
     */
    public function setBirthdateForDateTimeSetsBirthdate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setBirthdate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'birthdate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPositionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPosition()
        );
    }

    /**
     * @test
     */
    public function setPositionForStringSetsPosition()
    {
        $this->subject->setPosition('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'position',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPhoneReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPhone()
        );
    }

    /**
     * @test
     */
    public function setPhoneForStringSetsPhone()
    {
        $this->subject->setPhone('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'phone',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFaxReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFax()
        );
    }

    /**
     * @test
     */
    public function setFaxForStringSetsFax()
    {
        $this->subject->setFax('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fax',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMobileReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMobile()
        );
    }

    /**
     * @test
     */
    public function setMobileForStringSetsMobile()
    {
        $this->subject->setMobile('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'mobile',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail()
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'email',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSupporttextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSupporttext()
        );
    }

    /**
     * @test
     */
    public function setSupporttextForStringSetsSupporttext()
    {
        $this->subject->setSupporttext('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'supporttext',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSupportlinkReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSupportlink()
        );
    }

    /**
     * @test
     */
    public function setSupportlinkForStringSetsSupportlink()
    {
        $this->subject->setSupportlink('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'supportlink',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBuildingReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBuilding()
        );
    }

    /**
     * @test
     */
    public function setBuildingForStringSetsBuilding()
    {
        $this->subject->setBuilding('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'building',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRoomReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRoom()
        );
    }

    /**
     * @test
     */
    public function setRoomForStringSetsRoom()
    {
        $this->subject->setRoom('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'room',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBelongsToDefaultSysLanguageReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getBelongsToDefaultSysLanguage()
        );
    }

    /**
     * @test
     */
    public function setBelongsToDefaultSysLanguageForBoolSetsBelongsToDefaultSysLanguage()
    {
        $this->subject->setBelongsToDefaultSysLanguage(true);

        self::assertAttributeEquals(
            true,
            'belongsToDefaultSysLanguage',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDepartmentReturnsInitialValueForDepartment()
    {
        self::assertEquals(
            null,
            $this->subject->getDepartment()
        );
    }

    /**
     * @test
     */
    public function setDepartmentForDepartmentSetsDepartment()
    {
        $departmentFixture = new \HIVE\HiveExtContact\Domain\Model\Department();
        $this->subject->setDepartment($departmentFixture);

        self::assertAttributeEquals(
            $departmentFixture,
            'department',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCompanyReturnsInitialValueForCompany()
    {
        self::assertEquals(
            null,
            $this->subject->getCompany()
        );
    }

    /**
     * @test
     */
    public function setCompanyForCompanySetsCompany()
    {
        $companyFixture = new \HIVE\HiveExtContact\Domain\Model\Company();
        $this->subject->setCompany($companyFixture);

        self::assertAttributeEquals(
            $companyFixture,
            'company',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSocialMediaReturnsInitialValueForSocialMedia()
    {
        self::assertEquals(
            null,
            $this->subject->getSocialMedia()
        );
    }

    /**
     * @test
     */
    public function setSocialMediaForSocialMediaSetsSocialMedia()
    {
        $socialMediaFixture = new \HIVE\HiveExtContact\Domain\Model\SocialMedia();
        $this->subject->setSocialMedia($socialMediaFixture);

        self::assertAttributeEquals(
            $socialMediaFixture,
            'socialMedia',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTeufelsExtCountryReturnsInitialValueForCountry()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getTeufelsExtCountry()
        );
    }

    /**
     * @test
     */
    public function setTeufelsExtCountryForObjectStorageContainingCountrySetsTeufelsExtCountry()
    {
        $teufelsExtCountry = new \HIVE\HiveExtCountry\Domain\Model\Country();
        $objectStorageHoldingExactlyOneTeufelsExtCountry = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneTeufelsExtCountry->attach($teufelsExtCountry);
        $this->subject->setTeufelsExtCountry($objectStorageHoldingExactlyOneTeufelsExtCountry);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneTeufelsExtCountry,
            'teufelsExtCountry',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addTeufelsExtCountryToObjectStorageHoldingTeufelsExtCountry()
    {
        $teufelsExtCountry = new \HIVE\HiveExtCountry\Domain\Model\Country();
        $teufelsExtCountryObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $teufelsExtCountryObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($teufelsExtCountry));
        $this->inject($this->subject, 'teufelsExtCountry', $teufelsExtCountryObjectStorageMock);

        $this->subject->addTeufelsExtCountry($teufelsExtCountry);
    }

    /**
     * @test
     */
    public function removeTeufelsExtCountryFromObjectStorageHoldingTeufelsExtCountry()
    {
        $teufelsExtCountry = new \HIVE\HiveExtCountry\Domain\Model\Country();
        $teufelsExtCountryObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $teufelsExtCountryObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($teufelsExtCountry));
        $this->inject($this->subject, 'teufelsExtCountry', $teufelsExtCountryObjectStorageMock);

        $this->subject->removeTeufelsExtCountry($teufelsExtCountry);
    }
}
