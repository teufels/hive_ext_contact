<?php
namespace HIVE\HiveExtContact\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 * @author Yannick Aister <y.aister@teufels.com>
 */
class SocialMediaTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtContact\Domain\Model\SocialMedia
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtContact\Domain\Model\SocialMedia();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getFacebookReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFacebook()
        );
    }

    /**
     * @test
     */
    public function setFacebookForStringSetsFacebook()
    {
        $this->subject->setFacebook('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'facebook',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getYouTubeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getYouTube()
        );
    }

    /**
     * @test
     */
    public function setYouTubeForStringSetsYouTube()
    {
        $this->subject->setYouTube('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'youTube',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTwitterReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTwitter()
        );
    }

    /**
     * @test
     */
    public function setTwitterForStringSetsTwitter()
    {
        $this->subject->setTwitter('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'twitter',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLinkedInReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLinkedIn()
        );
    }

    /**
     * @test
     */
    public function setLinkedInForStringSetsLinkedIn()
    {
        $this->subject->setLinkedIn('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'linkedIn',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPinterestReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPinterest()
        );
    }

    /**
     * @test
     */
    public function setPinterestForStringSetsPinterest()
    {
        $this->subject->setPinterest('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'pinterest',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInstagramReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInstagram()
        );
    }

    /**
     * @test
     */
    public function setInstagramForStringSetsInstagram()
    {
        $this->subject->setInstagram('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'instagram',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getGooglePlusReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getGooglePlus()
        );
    }

    /**
     * @test
     */
    public function setGooglePlusForStringSetsGooglePlus()
    {
        $this->subject->setGooglePlus('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'googlePlus',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getXingReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getXing()
        );
    }

    /**
     * @test
     */
    public function setXingForStringSetsXing()
    {
        $this->subject->setXing('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'xing',
            $this->subject
        );
    }
}
