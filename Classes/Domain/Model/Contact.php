<?php
namespace HIVE\HiveExtContact\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_contact" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * Contact
 */
class Contact extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * gender
     *
     * @var int
     */
    protected $gender = 0;

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * firstname
     *
     * @var string
     */
    protected $firstname = '';

    /**
     * middlename
     *
     * @var string
     */
    protected $middlename = '';

    /**
     * lastname
     *
     * @var string
     */
    protected $lastname = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * focusX
     *
     * @var string
     */
    protected $focusX = '';

    /**
     * focusY
     *
     * @var string
     */
    protected $focusY = '';

    /**
     * birthdate
     *
     * @var \DateTime
     */
    protected $birthdate = null;

    /**
     * position
     *
     * @var string
     */
    protected $position = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';

    /**
     * fax
     *
     * @var string
     */
    protected $fax = '';

    /**
     * mobile
     *
     * @var string
     */
    protected $mobile = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * supporttext
     *
     * @var string
     */
    protected $supporttext = '';

    /**
     * supportlink
     *
     * @var string
     */
    protected $supportlink = '';

    /**
     * building
     *
     * @var string
     */
    protected $building = '';

    /**
     * room
     *
     * @var string
     */
    protected $room = '';

    /**
     * belongsToDefaultSysLanguage
     *
     * @var bool
     */
    protected $belongsToDefaultSysLanguage = false;

    /**
     * department
     *
     * @var \HIVE\HiveExtContact\Domain\Model\Department
     */
    protected $department = null;

    /**
     * company
     *
     * @var \HIVE\HiveExtContact\Domain\Model\Company
     */
    protected $company = null;

    /**
     * socialMedia
     *
     * @var \HIVE\HiveExtContact\Domain\Model\SocialMedia
     */
    protected $socialMedia = null;

    /**
     * teufelsExtCountry
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtCountry\Domain\Model\Country>
     */
    protected $teufelsExtCountry = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->teufelsExtCountry = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the gender
     *
     * @return int $gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the gender
     *
     * @param int $gender
     * @return void
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the firstname
     *
     * @return string $firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Sets the firstname
     *
     * @param string $firstname
     * @return void
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Returns the middlename
     *
     * @return string $middlename
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Sets the middlename
     *
     * @param string $middlename
     * @return void
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;
    }

    /**
     * Returns the lastname
     *
     * @return string $lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Sets the lastname
     *
     * @param string $lastname
     * @return void
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the focusX
     *
     * @return string $focusX
     */
    public function getFocusX()
    {
        return $this->focusX;
    }

    /**
     * Sets the focusX
     *
     * @param string $focusX
     * @return void
     */
    public function setFocusX($focusX)
    {
        $this->focusX = $focusX;
    }

    /**
     * Returns the focusY
     *
     * @return string $focusY
     */
    public function getFocusY()
    {
        return $this->focusY;
    }

    /**
     * Sets the focusY
     *
     * @param string $focusY
     * @return void
     */
    public function setFocusY($focusY)
    {
        $this->focusY = $focusY;
    }

    /**
     * Returns the birthdate
     *
     * @return \DateTime $birthdate
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Sets the birthdate
     *
     * @param \DateTime $birthdate
     * @return void
     */
    public function setBirthdate(\DateTime $birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * Returns the position
     *
     * @return string $position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the position
     *
     * @param string $position
     * @return void
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Returns the fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Returns the mobile
     *
     * @return string $mobile
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Sets the mobile
     *
     * @param string $mobile
     * @return void
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the supporttext
     *
     * @return string $supporttext
     */
    public function getSupporttext()
    {
        return $this->supporttext;
    }

    /**
     * Sets the supporttext
     *
     * @param string $supporttext
     * @return void
     */
    public function setSupporttext($supporttext)
    {
        $this->supporttext = $supporttext;
    }

    /**
     * Returns the supportlink
     *
     * @return string $supportlink
     */
    public function getSupportlink()
    {
        return $this->supportlink;
    }

    /**
     * Sets the supportlink
     *
     * @param string $supportlink
     * @return void
     */
    public function setSupportlink($supportlink)
    {
        $this->supportlink = $supportlink;
    }

    /**
     * Returns the building
     *
     * @return string $building
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Sets the building
     *
     * @param string $building
     * @return void
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    }

    /**
     * Returns the room
     *
     * @return string $room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Sets the room
     *
     * @param string $room
     * @return void
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * Returns the belongsToDefaultSysLanguage
     *
     * @return bool $belongsToDefaultSysLanguage
     */
    public function getBelongsToDefaultSysLanguage()
    {
        return $this->belongsToDefaultSysLanguage;
    }

    /**
     * Sets the belongsToDefaultSysLanguage
     *
     * @param bool $belongsToDefaultSysLanguage
     * @return void
     */
    public function setBelongsToDefaultSysLanguage($belongsToDefaultSysLanguage)
    {
        $this->belongsToDefaultSysLanguage = $belongsToDefaultSysLanguage;
    }

    /**
     * Returns the boolean state of belongsToDefaultSysLanguage
     *
     * @return bool
     */
    public function isBelongsToDefaultSysLanguage()
    {
        return $this->belongsToDefaultSysLanguage;
    }

    /**
     * Returns the department
     *
     * @return \HIVE\HiveExtContact\Domain\Model\Department $department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Sets the department
     *
     * @param \HIVE\HiveExtContact\Domain\Model\Department $department
     * @return void
     */
    public function setDepartment(\HIVE\HiveExtContact\Domain\Model\Department $department)
    {
        $this->department = $department;
    }

    /**
     * Returns the company
     *
     * @return \HIVE\HiveExtContact\Domain\Model\Company $company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the company
     *
     * @param \HIVE\HiveExtContact\Domain\Model\Company $company
     * @return void
     */
    public function setCompany(\HIVE\HiveExtContact\Domain\Model\Company $company)
    {
        $this->company = $company;
    }

    /**
     * Returns the socialMedia
     *
     * @return \HIVE\HiveExtContact\Domain\Model\SocialMedia $socialMedia
     */
    public function getSocialMedia()
    {
        return $this->socialMedia;
    }

    /**
     * Sets the socialMedia
     *
     * @param \HIVE\HiveExtContact\Domain\Model\SocialMedia $socialMedia
     * @return void
     */
    public function setSocialMedia(\HIVE\HiveExtContact\Domain\Model\SocialMedia $socialMedia)
    {
        $this->socialMedia = $socialMedia;
    }

    /**
     * Adds a Country
     *
     * @param \HIVE\HiveExtCountry\Domain\Model\Country $teufelsExtCountry
     * @return void
     */
    public function addTeufelsExtCountry(\HIVE\HiveExtCountry\Domain\Model\Country $teufelsExtCountry)
    {
        $this->teufelsExtCountry->attach($teufelsExtCountry);
    }

    /**
     * Removes a Country
     *
     * @param \HIVE\HiveExtCountry\Domain\Model\Country $teufelsExtCountryToRemove The Country to be removed
     * @return void
     */
    public function removeTeufelsExtCountry(\HIVE\HiveExtCountry\Domain\Model\Country $teufelsExtCountryToRemove)
    {
        $this->teufelsExtCountry->detach($teufelsExtCountryToRemove);
    }

    /**
     * Returns the teufelsExtCountry
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtCountry\Domain\Model\Country> $teufelsExtCountry
     */
    public function getTeufelsExtCountry()
    {
        return $this->teufelsExtCountry;
    }

    /**
     * Sets the teufelsExtCountry
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtCountry\Domain\Model\Country> $teufelsExtCountry
     * @return void
     */
    public function setTeufelsExtCountry(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $teufelsExtCountry)
    {
        $this->teufelsExtCountry = $teufelsExtCountry;
    }
}
