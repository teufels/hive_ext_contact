<?php
namespace HIVE\HiveExtContact\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_contact" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * The repository for Contacts
 */
class ContactRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtContact\\Domain\\Model\\Contact';
        $sUserFuncPlugin = 'tx_hiveextcontact';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * findByUidListOrderByListIfNotEmpty
     *
     * @param string String containing uids
     * @return \HIVE\HiveExtContact\Domain\Model\Contact
     */
    public function findByUidListOrderByListIfNotEmpty($uidList)
    {
        $uidArray = explode(',', $uidList);
        $result = [];
        foreach ($uidArray as $uid) {
            $result[] = $this->findByUid($uid);
        }

        //return only if not empty
        if ($result[0] != NULL){
            return $result;
        }
    }

    /**
     * Find by Uid
     *
     * @param int $uid
     * @return Contact
     */
    public function findByUid($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }
}
