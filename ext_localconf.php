<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtContact',
            'Contactlist',
            [
                'Contact' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Contact' => 'list, show'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtContact',
            'Contactlistforcurrentsyslanguagebycountries',
            [
                'Contact' => 'listForCurrentSysLanguageByCountries'
            ],
            // non-cacheable actions
            [
                'Contact' => 'listForCurrentSysLanguageByCountries'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    contactlist {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_contact') . 'Resources/Public/Icons/user_plugin_contactlist.svg
                        title = LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_contact_domain_model_contactlist
                        description = LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_contact_domain_model_contactlist.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextcontact_contactlist
                        }
                    }
                    contactlistforcurrentsyslanguagebycountries {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_contact') . 'Resources/Public/Icons/user_plugin_contactlistforcurrentsyslanguagebycountries.svg
                        title = LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_contact_domain_model_contactlistforcurrentsyslanguagebycountries
                        description = LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_contact_domain_model_contactlistforcurrentsyslanguagebycountries.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextcontact_contactlistforcurrentsyslanguagebycountries
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
