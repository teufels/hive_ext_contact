<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,city,zip,street,region,country,www,lat,lon,social_media',
        'iconfile' => 'EXT:hive_ext_contact/Resources/Public/Icons/tx_hiveextcontact_domain_model_company.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, city, zip, street, region, country, www, lat, lon, social_media',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, city, zip, street, region, country, www, lat, lon, social_media, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hiveextcontact_domain_model_company',
                'foreign_table_where' => 'AND tx_hiveextcontact_domain_model_company.pid=###CURRENT_PID### AND tx_hiveextcontact_domain_model_company.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'city' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'zip' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.zip',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'street' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'region' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.region',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'country' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.country',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'www' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.www',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lat' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.lat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lon' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.lon',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'social_media' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_company.social_media',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_hiveextcontact_domain_model_socialmedia',
                'minitems' => 0,
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],

    ],
];
