<?php

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$sModel = 'tx_hiveextcontact_domain_model_department';

$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'title';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'backend_title';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;