
plugin.tx_hiveextcontact_contactlist {
    view {
        templateRootPaths.0 = EXT:hive_ext_contact/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextcontact_contactlist.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_contact/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextcontact_contactlist.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_contact/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextcontact_contactlist.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextcontact_contactlist.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries {
    view {
        templateRootPaths.0 = EXT:hive_ext_contact/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_contact/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_contact/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hiveextcontact._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-ext-contact table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-ext-contact table th {
        font-weight:bold;
    }

    .tx-hive-ext-contact table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)



## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
//page {
//    includeCSS {
//        hive_ext_contact_less = {$plugin.tx_hiveextcontact_contactlist.settings.production.includePath.private}Assets/Less/hive_ext_contact.less
//        hive_ext_contact_less.media = all
//    }
//}

plugin {
    tx_hiveextcontact{

    # Standard PID aus den Konstanten
        persistence {
            storagePid = {$plugin.tx_hiveextcontact.persistence.storagePid}
        }

        model {
            HIVE\HiveExtContact\Domain\Model\Contact {
                persistence {
                    storagePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtContact\Domain\Model\Contact.persistence.storagePid}
                }
            }
            HIVE\HiveExtContact\Domain\Model\Abstract {
                persistence {
                    storagePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtContact\Domain\Model\Abstract.persistence.storagePid}
                }
            }
            HIVE\HiveExtContact\Domain\Model\Company {
                persistence {
                    storagePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtContact\Domain\Model\Company.persistence.storagePid}
                }
            }
            HIVE\HiveExtContact\Domain\Model\Department {
                persistence {
                    storagePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtContact\Domain\Model\Department.persistence.storagePid}
                }
            }
            HIVE\HiveExtContact\Domain\Model\SocialMedia {
                persistence {
                    storagePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtContact\Domain\Model\SocialMedia.persistence.storagePid}
                }
            }
        }

        classes {
            HIVE\HiveExtContact\Domain\Model\Contact.newRecordStoragePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtCountry\Domain\Model\Contact.persistence.storagePid}
            HIVE\HiveExtContact\Domain\Model\Abstract.newRecordStoragePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtCountry\Domain\Model\Abstract.persistence.storagePid}
            HIVE\HiveExtContact\Domain\Model\Company.newRecordStoragePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtCountry\Domain\Model\Company.persistence.storagePid}
            HIVE\HiveExtContact\Domain\Model\Department.newRecordStoragePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtCountry\Domain\Model\Department.persistence.storagePid}
            HIVE\HiveExtContact\Domain\Model\SocialMedia.newRecordStoragePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtCountry\Domain\Model\SocialMedia.persistence.storagePid}
        }

    }
}
